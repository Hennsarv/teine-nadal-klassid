﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetodJaFunktsioon
{
    class Program
    {
        static void Main(string[] args)
        {
            // siit pannakse programm käima

            int i = 7;
            string õ = "õieõeäia..";

            Tryki(i, "i"); // see on meetodi "kasutus" e "väljakutse"
                           // i ja "i" on väljakutse argumendid
            Tryki(õ, "õ");
            Tryki(i * i, "i ruut");
            int ihii = Liida100(4, 7);
            Tryki(Liida100(b: 17), "summa ja sada");

            Console.WriteLine(Liida("kaks arvu", 3, 4));
            Console.WriteLine(Liida("kolm arvu", 3, 4, 7));
            Console.WriteLine(Liida("null arvu"));

            int[] mitu = { 1, 2, 3, 4, 5, 6, 7 };
            Console.WriteLine(Liida("palju arve", mitu));

            Abiline.Meetod2();

            Abiline abi1 = new Abiline { Nimi = "esimene" };
            Abiline abi2 = new Abiline { Nimi = "teine" };
            abi1.Meetod1();
            abi2.Meetod1();


        }

        // siin on meetodi definitsioon
        static void Tryki(int o, string nimi) // o ja nimi on meetodi parameetrid
        {
            Console.WriteLine($"{nimi}={o} (int)");
        }
        static void Tryki(string o, string nimi) // o ja nimi on meetodi parameetrid
        {
            Console.WriteLine($"{nimi}={o} (string)");
           
        }

        
        static int Liida100(int a = 0, int b = 0, int c = 0)
        {
            return a + b*2 + c*3 + 200;
        }

        static int Liida(string mida, params int[] arvud )
        {
            Console.WriteLine($"liidan {mida}");
            int summa = 0;
            foreach (var x in arvud) summa += x;
            return summa;
        }


    }

    class Abiline
    {
        public string Nimi;

        public void Meetod1()
        {
            Console.WriteLine($"seda teeb esimene meetod {this.Nimi}");
        }

        public static void Meetod2()
        {
            Console.WriteLine($"seda teeb teine meetod");
        }



    }


}
