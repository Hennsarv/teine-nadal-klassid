﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverrideJaNew
{
    class Program
    {
        static void Main(string[] args)
        {
            A a = new A();
            A b = new B();
            A c = new C(); // proovi kui c muutuja on C tüüpi
            A d = new D(); // proovi kui d muutuja on C või D tüüpi

            a.MA();
            b.MA();
            c.MA();
            d.MA();

        }
    }

    class A
    {
        public virtual void MA() => Console.WriteLine("mina olen A"); 
    }
    class B : A
    {
        public override void MA() => Console.WriteLine("mina olen B");

    }
    class C : B
    {
        public virtual new void MA() => Console.WriteLine("mina olen C");
    }
    class D : C
    {
        public override void MA() => Console.WriteLine("mina olen D");

    }

}
