﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inimkond
{

    public enum Gender { Female, Male }

    public class Person
    {

        List<Person> People = new List<Person>();
        static int jooksevNumber = 0;

        public static Person Adam = new Person("Adam", "");
        public static Person Eva = new Person("Eva", "") { Kaasa = Adam };



        // siin on klassi (objekti) väljad
        private string _Nimi;
        private string _IK = "";
        private readonly int jrknr = ++jooksevNumber;


        private Person _Kaasa = null;
        public Person Ema { get; private set; }
        public Person Isa { get; private set; }


        public Person Kaasa
        {
            get => _Kaasa;
            set
            {
                this._Kaasa = value;
                value._Kaasa = this;
            }
        }

        public Person NewLaps(string nimi)
        {
            Person laps = new Person { Nimi = nimi };
            if (this.Gender == Gender.Male)
            {
                laps.Isa = this;
                laps.Ema = this.Kaasa;
            }
            else
            {
                laps.Ema = this;
                laps.Isa = this.Kaasa;
            }

            return laps;
        }


        // mis on konstruktor

        // see siin on kahe parameetriga konstruktor
        public Person(string nimi = "nimeta mats", string ik = "") // 3.
        {
            this.IK = ik;
            this.Nimi = nimi;
            //this.jrknr = ++jooksevNumber;
            People.Add(this);
        }



        // see siin on parameetriteta konstruktor
        // kui ühtegi konstruktorit pole, siis see tehakse automaatselt (vaikimisi konstruktor)
        // kui on kasvõi 1 konstruktor, siis vaikimisi parameetriteta konstruktorit ei tehta
        //public Person() : this("nimeta mats") {}    // 1. 
        // see konstruktor pöördub teise poole ja annab parameetriks nime


        // see on ühe parameetriga konstruktor
        // mis omakorda kutsub välja kahe parameetriga konstruktori
        //public Person(string nimi) : this(nimi, "") { } // 2.




        public string getIK() { return _IK; }
        public void setIK(string uusIK)
        {
            if (_IK == "") _IK = uusIK;
        }

        public string IK
        {
            get => _IK;
            // set { if (_IK == "") _IK = value; }
            set => _IK = _IK == "" ? value : _IK;
        }

        public string Nimi
        {
            get => _Nimi;
            set
            {
                //_Nimi = value; // selle asemel proovi kirjutada set meetod
                // mis teisendab nime suurte algustähtedega nimeks
                if (value == "") _Nimi = "unknown";
                else
                {
                    string[] osad = value.ToLower().Split(' ');
                    for (int i = 0; i < osad.Length; i++)
                    {
                        osad[i] = osad[i].Substring(0, 1).ToUpper() + osad[i].Substring(1);
                    }
                    _Nimi = String.Join(" ", osad);
                }

            }
        }


        // siit alates on klassi funktsioonid ja meetodid
        public DateTime BirthDate // annab inimese sünnikuupäeva
        {
            get
            {
                // IK.Substring(0,1) 1 v 2 1800, 3-4 1900, 5-6 2000
                // IK.Substring(1,2) + aasta
                // IK.Substring(3,2) kuu
                // IK.Substring(5,2) päev

                string sajand = "";
                switch (this._IK.Substring(0, 1))
                {
                    case "1": case "2": sajand = "18"; break;
                    case "3": case "4": sajand = "19"; break;
                    case "5": case "6": sajand = "20"; break;
                    case "7": case "8": sajand = "21"; break;
                }

                string kp = sajand
                    + _IK.Substring(1, 2) + "/"
                    + _IK.Substring(3, 2) + "/"
                    + _IK.Substring(5, 2);
                return DateTime.Parse(kp);
            }
        }

        public int Age // annab inimese vanuse
        => (DateTime.Today - BirthDate).Days * 4 / 1461;


        public Gender Gender => (Gender)(this._IK[0] % 2);
        // kuidas leida inimese sugu?


        // siit alates overraidid (hiljem seletame, mis need on)
        // kuidas inimene stringiks teha
        public override string ToString() => $"Inimene nimega {Nimi} (ik: {_IK} ) numbriga:{jrknr}";

    }

    public class Joosep
    {

    }


}
