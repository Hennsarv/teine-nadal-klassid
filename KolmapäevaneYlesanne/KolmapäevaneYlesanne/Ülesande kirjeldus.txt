﻿Kui lõuna söödud, siis teeme ise ka proovi

Tee kolm klassi Inimene, Õpetaja, Õpilane

Inimene (Nimi, IK, Sugu, Vanus, Sünnipäev)
Õpetaja : Inimene (Aine, mida õpetab, klass, mida juhatab)
Õpilane : Inimene (klass, kus õpib)

Õpetaja õpetab ühte ainet (kui ei ole sellist ainet, pole ta ju õpetaja)
Õpetaja võib olla klassijuhataja (ühe klassi juhataja), aga ei pruugi
Õpilane õpib mingis kindlas klassis (muidu ta poleks õpilane)

Kõik inimesed peaks olema kuskil nimekirjas (dictionary vist)
sama isikukoodiga kahte inimeste ei saa olla

Testandmeteks tee kaks tekstifaili õpilased.txt ja õpetajad.txt
Õpilased (Henn,35503070211,1A) - Henn, isikukoodiga, õpib 1A klassis
Õpetajad (Henn,35503070211,Matemaatika,1A) - Henn, isikukoodiga, 
	on Matemaatika õpetaja ja 1A klassi juhataja

Testimiseks tee Main meetod, mis
a) loeb mõlemad failid ja teeb neist õpetajad
b) teeb nimekirja kooli personalist
c) teeb nimekirja õpetajatest
d) teeb nimekirja õpilastest (ei pea klasside kaupa, aga kui oskad, siis võid teha)

NB! mõtle, aruta naabriga, proovi ise, kui tekkib tõrge või segadus, küsi!
mõtle, mis järjekorras sa asjad teed (prioriteet)