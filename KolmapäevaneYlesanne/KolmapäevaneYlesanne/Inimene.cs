﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolmapäevaneYlesanne
{
    class Inimene
    {
        protected static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        public static Inimene GetByIk(string ik)
        
            //if (_Inimesed.ContainsKey(ik)) return _Inimesed[ik];
            //else return null;
             => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : null;

        

        public string Nimi { get; set; }
        public string IK { get; private set; } // ? miks private?

        // vahva on teha kohe parameetritega konstruktorid
        // aga see ei ole kohustuslik - maitse küsimus
        protected Inimene(string ik, string nimi)
        {
            IK = ik; Nimi = nimi;
            _Inimesed.Add(ik, this);
        }

        public static Inimene Create(string ik, string nimi)
        => _Inimesed.ContainsKey(ik) ? _Inimesed[ik]: new Inimene(ik, nimi);
        

        public override string ToString() // overriditud meetod on kohe virtual
        {
            return $"Inimene {Nimi} (IK: {IK})";
        }
    }

    class Õpetaja : Inimene
    {

        // mille poolest Õpetaja erineb Inimesest?
        public string Aine { get; private set; } // aine, mida õpetab
        public string Klass { get; set; } = "";  // klass, mida juhatab

        // klassi ma õpetajal konstruktorisse ei pane - miks?
        protected Õpetaja(string ik, string nimi, string aine) : base(ik, nimi) => Aine = aine;

        public static Inimene Create(string ik, string nimi, string aine)
         => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : new Õpetaja(ik, nimi, aine);


        public override string ToString()
        {
            return $"{Aine} õpetaja {Nimi} (IK: {IK})";
        }

    }

    class Õpilane : Inimene
    {
        // mille poolest Õpilane erineb Inimesest?
        public string Klass { get; set; } // klass, kus õpib

        public Õpilane(string ik, string nimi, string klass) : base(ik, nimi) => Klass = klass;

        public override string ToString()
        {
            return $"{Klass} õpilane " + base.ToString();
        }
    }
}
