﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolmapäevaneYlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i1 = Inimene.Create("35503070211", "Henn Sarv");

            //Console.WriteLine(i);

            Inimene ants = Õpetaja.Create("33303030333", "Ants Saunamees", "matemaatika");
            //Console.WriteLine(ants);

            Õpilane malle = new Õpilane("61005060111", "Malle Mallikas", "1A");
            //Console.WriteLine(malle);

            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);

            Console.WriteLine(Inimene.GetByIk("35503070211"));

            Inimene i2 = null;
            if ((i2 = Inimene.GetByIk("35503070213")) == null)
            i2 = Inimene.Create("35503070213", "Sarviktaat"); // ???
            Console.WriteLine(i2);

        }
    }
}
