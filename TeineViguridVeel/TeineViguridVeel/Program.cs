﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineViguridVeel
{
    class Program
    {
        static void Main(string[] args)
        {
            Lihtne rebane = new Lihtne { Nimi = "Rebane" };
            Lihtne jänene = new Lihtne { Nimi = "Jänes" };

            rebane.Ohoo();
            Lihtne.Ohoo(jänene);

            E.Ehee(jänene);
            rebane.Ehee();

            "Henn läheb lõunale"
                .ToUpper()
                .Replace("HENN", "Sarvik")
                .Tryki();

            List<int> palgad = new List<int> { 1000, 1200, 1400, 1800, 300 };
            Console.WriteLine(palgad.Summa());

            int[] arvud = { 1, 7, 2, 8, 3, 5, 6, 2, 5, 9, 11 };

            //Func<int, bool> kj3 = x => x % 3 == 0;

            foreach(var x in arvud

                    //.Paaris()
                    //.Kas(x => x % 2 == 0)
                    //.Kas(x => x < 5)
                    //.Where(x => x > 5)
                    .SkipWhile(x => x != 3)
                    .Select(x => x*x)
                    //.Take(3)
                    ) Console.WriteLine(x);

            Console.WriteLine(arvud
                .Where(x => x % 2 == 0)
                .Where(x => x > 100)
                .DefaultIfEmpty()
                .Average()
                );

            var misasjadneedon = arvud.Where(x => x % 2 == 0).ToList();

            var segadus = arvud.Select(x => new { arv = x, temaRuut = x * x }).ToList();

            segadus.ForEach(x => Console.WriteLine(x));

            Func<int, int> miskiF = (x) => x * x + x;
            Func<int, int, int> teineF = (x,y) => x * y + x / y;


            Console.WriteLine(miskiF(10));

            foreach(var x in palgad
                .Select ( x => x < 500 ? 0 : (x-500) * 1.2)
                ) Console.WriteLine(x);

            var q = from x in arvud
                    where x % 2 == 0
                    orderby x descending
                    select new { arv = x, ruut = x * x };
            foreach (var x in q) Console.WriteLine(x);

            var q1 = arvud
                    .Where(x => x % 2 == 0)
                    .OrderByDescending(x => x)
                    .Select(x => new { arv = x, ruut = x * x });

            foreach (var x in q1) Console.WriteLine(x);



        }
    }

    class Lihtne
    {
        public string Nimi;

        public void Ohoo() => Console.WriteLine($"{this.Nimi} ütleb Ohoo");

        public static void Ohoo(Lihtne lihtne) => Console.WriteLine($"{lihtne.Nimi} ütleb Ohoo");
    }

    static class E  // helper, utiliti - kasulike lisavidinate klass
    {
        public static void Ehee(this Lihtne lihtne) => Console.WriteLine($"{lihtne.Nimi} ütleb Ehee");

        public static void Tryki(this string sõna) => Console.WriteLine(sõna);

        public static int Summa(this IEnumerable<int> arvud)
        {
            int summa = 0;
            foreach (var x in arvud) summa += x;
            return summa;
        }

        public static IEnumerable<int> Paaris(this IEnumerable<int> arvud)
        {
            foreach (var x in arvud)
                if (x % 2 == 0) yield return x;
        }

        public static bool KasPaaris(int arv) => arv % 2 == 0;
        public static bool KasPaaritu(int arv) => arv % 2 == 1;

        public static IEnumerable<int> Kas(this IEnumerable<int> arvud, Func<int, bool> f)
        {
            foreach (var x in arvud)
                if (f(x)) yield return x;
        }

        public static IEnumerable<int> Võta(this IEnumerable<int> arvud, int mitu)
        {
            int i = 0;
            foreach (var x in arvud)
            {
                if (++i > mitu) break;
                yield return x;
            }
        }
        public static IEnumerable<int> Jäta(this IEnumerable<int> arvud, int mitu)
        {
            int i = 0;
            foreach (var x in arvud)
            {
                if (++i > mitu) 
                yield return x;
            }
        }


    }


}
