﻿klassi kirjeldamisel (ehitamisel) kasuta alati sellist järjekorda

1. Kõik väljad

1.a) staatilised väljad
1.b) objekti väljad

2. Kõik funktsioonid ja meetodid
2.a) konstruktorid (static siis objekti omad) NB! hiljem lisandub siia veel destruktor
2.b) kõik propertyd (väike erand lubatud - sellest hiljem) - statikud ja siis objekti omad
2.c) funktsioonid 
2.d) meetodid

3. overrided ja implemented meetodi lõpus (interfeiside kaupa)

iga grupi sees - vastavalt juurdepääsule

kõigepealt private
vahepeal internal ja protected mis iganes asjad need ka ei ole
viimasena public asjad