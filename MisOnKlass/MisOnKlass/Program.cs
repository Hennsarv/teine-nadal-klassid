﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace MisOnKlass
{
    class Program
    {
        static void Main(string[] args)
        {

            //Person henn = new Person  // see sain numbri 0
            //{
            //    Nimi = "henn sarv",
            //    IK = "35503070211"
            //};

            //Person maris = new Person
            //{
            //    Nimi = "Maris Sarv"
            //};
            //henn.Kaasa = maris;
            //Console.WriteLine(maris.Kaasa);

            //var ml = henn.NewLaps("Mai-Liis");
            //Console.WriteLine(ml.Ema);

            //Person.Adam.NewLaps("Kain");
            //Person.Adam.NewLaps("Abel");
            //Person.Adam.NewLaps("Seth");




            string filename = @"..\..\Nimekiri.txt";
            string[] linesFromFile = System.IO.File.ReadAllLines(filename);

            List<Person> listOfPeople = new List<Person>();

            // siin me loeme failist listi
            foreach (string lineFromFile in linesFromFile)
            {
                Person uus = new Person
                {
                    Nimi = lineFromFile.Split(',')[0],
                    IK = lineFromFile.Split(',')[1]
                };

                listOfPeople.Add(uus);
            }
            // siin on meil list valmis


            // nüüd proovime selle valmis loetud listi välja trükkida
            foreach (Person person in listOfPeople)
            {
                Console.WriteLine(person);
            }
            // siin on see välja trükitud

            double sum = 0;
            foreach (var x in listOfPeople) sum += x.Age;
            Console.WriteLine($"keskmine vanus on {sum / listOfPeople.Count}");

            Person keegi = listOfPeople[4];



        }
    }



}
