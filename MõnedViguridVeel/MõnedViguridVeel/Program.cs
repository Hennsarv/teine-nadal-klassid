﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MõnedViguridVeel
{
    class Program
    {
        static void Main(string[] args)
        {
            Õpilane õ = new Õpilane { Nimi = "Malle", Klass = "1A" };
            Inimene i = new Inimene { Nimi = "Henn" };

            Inimene x = i; //Console.WriteLine(x);
            Inimene y = õ; //Console.WriteLine(y);

            List<Inimene> Inimesed = new List<Inimene> { x, y };

            foreach (var v in Inimesed)
            {
                if (v is Õpilane võ) Console.WriteLine($"{võ.Nimi} on {võ.Klass} klassi õpilane");
                else Console.WriteLine($"{v.Nimi} ei ole õpilane");
            }

//            Õpilane xõ = x as Õpilane;
            Õpilane xõ = x is Õpilane xxõ ? xxõ : null;
            Console.WriteLine(xõ);

            foreach (var v in Inimesed)
            {
                Console.WriteLine($"{v.Nimi} õpib klassis {(v as Õpilane)?.Klass??"elukoolis"}");
            }

        }
    }

    class Inimene
    {
        public string Nimi { get; set; }
        public override string ToString() => $"Inimene {Nimi}";
    }

    class Õpilane : Inimene
    {
        public string Klass { get; set; }
        public override string ToString() => $"{Klass} õpilane {Nimi}";

        public void UusKlass(string uusklass) => Klass = uusklass;
    }
}
