﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeneKool
{
    class Program
    {
        static void Main(string[] args)
        {


            // teeme proovi, kas Inimese klass toimib
            #region proovimine
            //Inimene henn = new Inimene("35503070211")
            //{
            //    Nimi = "henn sarv",
            //    Aine = "matemaatika",
            //    Klass = "1A"
            //};
            //Inimene pille = new Inimene("48501230000")
            //{
            //    Nimi = "pille riin",
            //    Klass = "2B"
            //};
            //pille.Vanemad.Add(henn);
            //henn.Lapsed.Add(pille);
            //Inimene malle = new Inimene("49901010111")
            //{
            //    Nimi = "Malle mallikas",
            //    Klass = "1A"
            //};
            //Inimene ülle = new Inimene("466010203045")
            //{
            //    Nimi = "ülle ülane",
            //    Aine = "eesti keel"
            //}; 
            //foreach (var i in Inimene.Inimesed) Console.WriteLine(i);
            #endregion


            // 1. loeme sisse õpilased
            E.ReadLines("Õpilased")
                //.ToList().ForEach(x => Console.WriteLine(x));
                .Select(x => x.Split(','))
                .Select(x => new Inimene(x[0]) { Nimi = x[1], Klass = x[2] })
                .ToList()
                ;
            // 2. loeme sisse õpetajad
            "Õpetajad".ReadLines()
                .Select(x => (x + ",").Split(','))
                .Select(x => new Inimene(x[0]) { Nimi = x[1], Aine = x[2], Klass = x[3] })
                .ToList();
            // 3. loeme sisse lapsevanemad
            var kesOnKelleLaps = "KesOnKelleVanemad".ReadLines()
                .Select(x => x.Split(','))
                .Select(x => new { Ik = x[0], Nimi = x[1], Lapsed = x.Skip(2).ToArray() })
                .ToList();
            foreach(var x in kesOnKelleLaps)
            {
                Inimene i = Inimene.Get(x.Ik) ?? (new Inimene(x.Ik) { Nimi = x.Nimi });
                foreach (var l in x.Lapsed) i.LisaLaps(l); 
            }

            // siin trükime kõik inimesed välja igaks juhuks
            foreach (var i in Inimene.Inimesed) Console.WriteLine(i);

            // 4. loeme sisse hinded

            var hinded = 
            "Hinded".ReadLines()
                .Select(x => x.Split(','))
                .Select(x => new AineHinne { ÕpetajaIK = x[0], ÕpilaseIK = x[1], Aine = x[2], Hinne = (Hinne)(int.Parse(x[3])) })
                .ToList();

            hinded.ForEach(x => Console.WriteLine(x));

            // 5. proovime vastata küsimustele / lahendada ülesannet

            // Õpilase keskmine hinne

            foreach( var x in 
            hinded.ToLookup(x => x.ÕpilaseIK)
                .Select(x => new { Inimene = Inimene.Get(x.Key), Keskmine = x.Average(y => y.HinneNum) })
                .Select(x => new { Nimi = x.Inimene?.Nimi ?? "tundmatu", Klass = x.Inimene?.Klass ?? "xx", x.Keskmine })
                .ToLookup(x => x.Klass)
                )
            {
                Console.WriteLine($"Klassi {x.Key} õpilaste hinded:" );
                foreach(var y in x)
                    Console.WriteLine($"\tõpilasel {y.Nimi} on keskmine hinne {y.Keskmine}");
            }

        }

    }

    static class E // siia teen asju, et mugavam oleks
    {
        public static string ToProper(this string nimi)
            => nimi == "" ? "" : 
            nimi.Split(' ')
                .Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower())
                .Join();

        // Hennu laiskus - tahab String.Join funktsiooni kasutada .Join() kujul
        public static string Join(this IEnumerable<string> nimed, string eraldaja = " ")
            => String.Join(eraldaja, nimed);

        public static string FileName(this string name) => $@"..\..\{name}.txt";

        public static IEnumerable<string> ReadLines(this string name) 
            => System.IO.File.ReadAllLines(name.FileName())
                .Select(x => x.Replace(", ", ",").Trim())
                .Where(x => x != "");
    }

    class Inimene
    {
        // millest koosneb
        private static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        public string IK { get; private set; }
        private string _Nimi;
        public string Nimi { get => _Nimi; set => _Nimi = value.ToProper(); }
        public string Aine { get; set; } = "";
        public string Klass { get; set; } = "";
        public bool KasAdmin { get; private set; } = false;

        public List<Inimene> Lapsed = new List<Inimene>();
        public List<Inimene> Vanemad = new List<Inimene>();

        public Inimene(string ik)
        {
            IK = ik;
            if (!_Inimesed.ContainsKey(ik)) _Inimesed.Add(ik, this);
        }

        public bool KasÕpetaja => Aine != "";
        public bool KasÕpilane => !KasÕpetaja && Klass != "";
        public bool KasLapsevanem => Lapsed.Count > 0;

        public static Inimene Get(string ik) => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : null;

        public void LisaLaps(string ik)
        {
            Inimene laps = Get(ik);
            if (laps != null) { Lapsed.Add(laps); laps.Vanemad.Add(this); }
        }

        public override string ToString()
         => (KasÕpetaja ? $"{Aine} õpetaja " + (Klass == "" ? "" : ($"{Klass} klassi juhataja ")): "") +
            (KasÕpilane ? $"{Klass} õpilane " : "") + Nimi
            + (KasLapsevanem ? $" (Lapsed: {Lapsed.Select(x => x.Nimi).Join(", ")})" : "")
            + (Vanemad.Count > 0 ? $" (Vanemad: {Vanemad.Select(x => x.Nimi).Join(", ")})" : "")
            ;
    }

    public enum Hinne { Puudulik, Kasin, Rahuldav, Hea, Vägahea, Suurepärane}

    class AineHinne
    {
        public string ÕpetajaIK { get; set; }
        public string ÕpilaseIK { get; set; }
        public string Aine { get; set; }
        public Hinne Hinne { get; set; }

        public int HinneNum => (int)Hinne;

        public override string ToString() 
            => $"{Inimene.Get(ÕpilaseIK)?.Nimi ?? "tundmatu õpilane"} hinne aines {Aine} on {Hinne} ({HinneNum}) õpetaja: {Inimene.Get(ÕpetajaIK)?.Nimi ?? "tundmatu õpetaja"}";
        
    }
}
