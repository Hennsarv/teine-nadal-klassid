﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpordipäevaÜlesanne
{
    static class E
    {
        public static void ForEach<T>(this IEnumerable<T> kogum, Action<T> a)
        { foreach (T t in kogum) a(t); }

        public static string[] Tykeldaja(string x)
        => x.Split(',').Select(y => y.Trim()).ToArray();

        
    }

    struct ProtokolliRida
    {
        public string Nimi;
        public int Distants;
        public double Aeg;
        public double Kiirus => Distants / Aeg;
        public override string ToString() => $"{{ Nimi = {Nimi}, Distants = {Distants}, Aeg = {Aeg}, Kiirus {Kiirus} }}";
    }
    class Program
    {
        static void Main(string[] args)
        {



            string filename = @"..\..\spordipäeva protokoll.txt";
            #region lambdadega
            var protokoll = System.IO.File.ReadAllLines(filename)
         .Skip(1)
         .Where(x => x.Trim() != "")
         .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
         //.Select(E.Tykeldaja)
         .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = double.Parse(x[2]) })
         .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants / x.Aeg })
         .ToList()
         ;
            protokoll.ForEach(x => Console.WriteLine(x)); // see rida on testimiseks

            #endregion

            #region ilma lambadeta sama asi
            List<ProtokolliRida> protokoll2 = new List<ProtokolliRida>();
            string[] loetudRead = System.IO.File.ReadAllLines(filename);
            for (int i = 1; i < loetudRead.Length; i++)
            {
                if (loetudRead[i].Trim() == "") continue;
                string[] reatükid = loetudRead[i].Split(',');
                ProtokolliRida pr = new ProtokolliRida
                {
                    Nimi = reatükid[0].Trim(),
                    Distants = int.Parse(reatükid[1].Trim()),
                    Aeg = double.Parse(reatükid[2].Trim())
                };
                protokoll2.Add(pr);
            }
            //foreach (var x in protokoll2) Console.WriteLine(x);

            #endregion
            // kes on kõige kiirem jooksja

            Console.WriteLine(protokoll
                .OrderByDescending(x => x.Kiirus)
                .Take(1)
                .Select(x => $"\nkõige kiirem jooksja on {x.Nimi} tema kiirus on {x.Kiirus}")
                .Single()
                );

            // kes on kõige kiirem sprinter

            Console.WriteLine(protokoll
                .Where(x => x.Distants == 100)
                .OrderByDescending(x => x.Kiirus)
                .Take(1)
                .Select(x => $"\nkõige kiirem sprinter on {x.Nimi} tema kiirus {x.Distants} meetril on {x.Kiirus}")
                .Single()
                );

            // kes on kõige lühema maa kiireim (kui me ei tea, mis distants oli lühim)

            var minDistants = protokoll.Min(y => y.Distants);
            Console.WriteLine(protokoll
                .Where(x => x.Distants == minDistants)
                .OrderByDescending(x => x.Kiirus)
                .Take(1)
                .Select(x => $"\nkõige lühema maa sprinter on {x.Nimi} tema kiirus {x.Distants} meetril on {x.Kiirus}")
                .Single()
                );

            var stab = protokoll
                .ToLookup(x => x.Nimi) // räägin pisut, mida teeb ToLookup()
                .Select(x => new { Nimi = x.Key, Mitu = x.Count(), Kiireim = x.Max(y => y.Kiirus), Aeglaseim = x.Min(y => y.Kiirus) })
                // iga nime taha arvutatakse tema jooksude arv, kiireim ja aeglaseim aeg
                .Where(x => x.Mitu > 1) // meid huvitavad mitu distantsi jooksnud
                .OrderBy(x => Math.Abs(x.Kiireim - x.Aeglaseim)) // sorteerime max-min vahede järgi
                .FirstOrDefault(); // miks default?

            if (stab != null)
                Console.WriteLine($"Kõige stabiilsem oli {stab.Nimi} tema suurim kiirus oli {stab.Kiireim} ja väiksem {stab.Aeglaseim}");

        }
    }
}
