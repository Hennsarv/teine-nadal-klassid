﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivateInimene
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = Inimene.Create("35503070211");
            Inimene keegiTeine = Inimene.Create("44404040000");
            keegiTeine.Nimi = "Malle Mallikas";

            Inimene sarvik = Inimene.Create("35503070211");
            sarvik.Nimi = "Sarviktaat";

            Inimene.Create("60001020345").UusNimi("");
            Inimene.Create("60001020345").UusNimi("Valli Valge");



            foreach (var x in Inimene.Inimesed)
                Console.WriteLine(x.Nimi);

        }
    }
}
