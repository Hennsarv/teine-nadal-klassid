﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivateInimene
{
    class Inimene
    {
        static Dictionary<String, Inimene> _Inimesed = new Dictionary<string, Inimene>();

        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        public string Nimi = "";
        public readonly string Ik;

        private Inimene(string ik) // private konstruktor
        {
            this.Ik = ik;
            _Inimesed.Add(ik, this);
        }

        public void UusNimi(string uusNimi) 
            => Nimi = Nimi == "" ? uusNimi : Nimi;

        public static Inimene Create(string ik)
        
            //if (_Inimesed.ContainsKey(ik)) return _Inimesed[ik];
            //else return new Inimene(ik);
            => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : new Inimene(ik);

        public static Inimene Get(string ik)
            => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : null;
        
    }
}
