﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom l = new Metsloom("krokodill");
            Console.WriteLine(l);
            l.TeeHäält();


            Koduloom k = new Koduloom("Ämblik")
            {
                Nimi = "Aadu"
            };
            Console.WriteLine(k);
            k.TeeHäält();
            k.Toida("sääsk");

            Kass murri = new Kass
            {
                Nimi = "Murri",
                Tõug = "siiami"
            };
            Console.WriteLine(murri);
            murri.Silita();
            murri.TeeHäält();
            murri.SikutaSabast();
            murri.TeeHäält();
            murri.Toida("Viscy");

            Koer pontu = new Koer
            {
                Nimi = "Pontu"

            };

            object o = pontu;
            Console.WriteLine("kes on pontu:\n");
            if (o is Loom) Console.WriteLine("pontu on loom");
            if (o is Metsloom) Console.WriteLine("pontu on metsloom");
            if (o is Koduloom) Console.WriteLine("pontu on koduloom");
            if (o is Koer) Console.WriteLine("pontu on koer");
            if (o is ISöödav) Console.WriteLine("pontu on söödav");
            if (o is Object) Console.WriteLine("pontu on object");



            List<object> loomaaed = new List<object>
            {
                k,
                l,
                murri,
                pontu,
                new Sepik(),
                "konn"
            };


            SortedSet<Koer> koerad = new SortedSet<Koer>
            {
                new Koer { Nimi = "Pauka" },
                new Koer { Nimi = "Adalbert" },
                new Koer { Nimi = "Coffy" },
                new Koer { Nimi = "Pauka" },
                new Koer { Nimi = "Lulla" },
                new Koer { Nimi = "Pallu" },
            };

            foreach (var x in koerad) Console.WriteLine(x);

            Console.WriteLine("\ntrükime välja loomaaia\n");
            foreach (var x in loomaaed) Console.WriteLine(x);

            Sepik sepik = new Sepik();
            //sepik.Süüakse();

            //pontu.Süüakse();

            foreach (var x in loomaaed) Lõuna(x);


        }

        public static void Lõuna(object midagi)
        {
            if (midagi is Loom l) l.Toida("rohtu");

            if (midagi is ISöödav söödav) söödav.Süüakse();
            else Console.WriteLine($"täna jääme nälga kuna {midagi} pole söödav");
        }
    }
    // ma olen hetkel laisk ja teen klassid sama faili sisse
    abstract class Loom
    {
        // ma esialgu ei viitsi mõelda publik-private - 
        // teen alguses kõik vidinad publik
        public string Liik { get; set; } = "";  // väli või property

        public Loom(string liik) => Liik = liik; // konstruktor

        public override string ToString() => $"loom liigist {Liik}"; // funktsioon

        public virtual void TeeHäält() => Console.WriteLine($"{this} teeb koledat häält"); // meetod

        // see siin on abstractne meetod - ainult abstractses klassis
        public abstract void Toida(string söök);

    }

    class Metsloom : Loom
    {
        public Metsloom(string liik = "tundmatu") : base(liik) { }

        public override void Toida(string söök)
        {
            Console.WriteLine($"Jätame metsa alla pisut {söök}");
        }
    }

    class Koduloom : Loom
    {
        public string Nimi { get; set; } = "nimeta";

        public Koduloom(string liik) : base(liik) { }

        public override void Toida(string söök)
        {
            Console.WriteLine($"koduloomale {Nimi} jätame talli pisut {söök} nosimiseks");
        }
    }

    sealed class Kass : Koduloom
    {
        public Kass() : base("kass") { }

        public string Tõug { get; set; } = "";
        private bool tuju = false;

        public void Silita() => tuju = true;
        public void SikutaSabast() => tuju = false;

        public override void TeeHäält()  // ümber defineeritud baasklassi virtuaalne meetod
        {
            if (tuju)
                Console.WriteLine($"{Tõug} kass {Nimi} loob nurru");
            else
                Console.WriteLine($"{Tõug} kass {Nimi} kräunub");

        }

        public override string ToString() => $"{Tõug} kiisu {Nimi}";

        public override void Toida(string söök)
        {
            Console.WriteLine($"Paneme kausi sisse kassikrõbinad {söök} {Nimi} jaoks");
        }
    }

    class Koer : Koduloom, ISöödav, IComparable
    {
        public Koer() : base("koer") { }

        public string Bread { get; set; } = "granze";

        public int CompareTo(object obj)
        {
            return obj is Koer k ? this.Nimi.CompareTo(k.Nimi) : -1;
        }

        public void Süüakse()
        {
            Console.WriteLine($"koer {Nimi} pistetakse nahka");
        }

        public override void TeeHäält()
        {
            Console.WriteLine($"koer {Nimi} haugub");
        }

        public override void Toida(string söök)
        {
            Console.WriteLine($"koer {Nimi} järagu {söök} konti");
        }

        public override string ToString() => $"Koer {Nimi}";
        
    }

    interface ISöödav
    {
        void Süüakse();
    }

    class Sepik : ISöödav
    {
        public void Süüakse()
        {
            Console.WriteLine("keegi nosib sepikut");
        }
    }

}
